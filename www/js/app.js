angular.module('starter', [
    'ionic',
    'starter.controllers',
    'ngCordova',
    'ionicLazyLoad',
    'GoogleMapsNative',
    '500tech.simple-calendar',
    'ionic-ratings',
    'pascalprecht.translate',
    'ngMap'])
    .run(function ($ionicPlatform, $rootScope, $cordovaGeolocation, $state) {
        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
            $rootScope.UserLoggedIn = false;
            $rootScope.language = localStorage.getItem('mwaeed:language') || 'en';

            var posOptions = {
                timeout: 10000,
                enableHighAccuracy: false
            };

            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) { }, function (err) {
                    console.log(err);
                });

            /*

                     navigator.splashscreen.hide();

            */

            // Push Notification
            // Enable to debug issues.
            // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

            var notificationOpenedCallback = function (jsonData) {
                //alert("ONE SIGNAL"+JSON.stringify(jsonData));
                console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
                $rootScope.$emit('notificationOpened', JSON.stringify(jsonData));
                $state.go('tab.myappointment');
            };

            if (window.plugins && window.plugins.OneSignal) {
                window.plugins.OneSignal
                    .startInit("72bccb7f-81a2-47c3-933a-8fc46b4155ab")
                    .handleNotificationOpened(notificationOpenedCallback)
                    .endInit();
            }

            // Call syncHashedEmail anywhere in your app if you have the user's email.
            // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
            // window.plugins.OneSignal.syncHashedEmail(userEmail);

        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $compileProvider, $httpProvider) {
        $ionicConfigProvider.tabs.position('top');
        if (ionic.Platform.isAndroid()) {
            $ionicConfigProvider.scrolling.jsScrolling(false);
        }
        $ionicConfigProvider.views.transition('ios');
        $ionicConfigProvider.tabs.style('ios');

        $compileProvider.debugInfoEnabled(false);
        $ionicConfigProvider.backButton.text('');
        $ionicConfigProvider.backButton.previousTitleText(false);
        $ionicConfigProvider.form.toggle("large");
        $ionicConfigProvider.views.swipeBackEnabled(false);





        $stateProvider

            // setup an abstract state for the tabs directive
            .state('tab', {
                cache: false,
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html',
                controller: 'TabCtrl'

            })
            .state('start', {
                abstract: true,
                url: '/start',
                templateUrl: 'templates/start.html'
            })
            .state('start.help', {
                url: '/help',
                views: {
                    'st': {
                        templateUrl: 'templates/start-help.html',
                        controller: 'HelpCtrl'
                    }
                }
            })
            .state('start.login', {
                url: '/login',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/start-login.html',
                        controller: 'LoginCtrl'
                    }
                }
            }).state('start.forgot', {
                url: '/forgot',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/start-forgot.html',
                        controller: 'ForgotCtrl'
                    }
                }
            }).state('start.register', {
                url: '/register',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/start-register.html',
                        controller: 'LoginCtrl'
                    }
                }
            })
            .state('start.confirmationaccount',{
                url: '/confirmationaccount/:email/:password',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/confirm-account.html',
                        controller: 'confirmationAccount'
                    }
                }
            })
            .state('tab.dash', {
                url: '/dash',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/tab-dash.html',
                        controller: 'DashCtrl'
                    }
                }
            }).state('tab.search', {
                url: '/search',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/tab-search.html',
                        controller: 'SearchCtrl'
                    }
                }
            }).state('tab.changepassword', {
                url: '/changepassword',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/tab-changepassword.html',
                        controller: 'ChangePasswordCtrl'
                    }
                }
            }).state('tab.calendar', {
                url: '/calendar',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/tab-calendar.html',
                        controller: 'CalendarCtrl'
                    }
                }
            }).state('tab.account', {
                url: '/account',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/account.html',
                        controller: 'AccountCtrl'
                    }
                }
            }).state('tab.speciality', {
                url: '/speciality/:date',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/speciality.html',
                        controller: 'SpecialityCtrl'
                    }
                }
            }).state('tab.subspeciality', {
                url: '/subspeciality/:id',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/sub-speciality.html',
                        controller: 'SubSpecialityCtrl'
                    }
                }
            }).state('tab.pay', {
                url: '/pay',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/pay.html',
                        controller: 'PaymentCtrl'
                    }
                }
            }).state('tab.myappointment', {
                cache: false,
                url: '/myappointment',
                views: {
                    'st': {
                        templateUrl: 'templates/myappointment-listing.html',
                        controller: 'MyAppointmentCtrl'
                    }
                }
            })
            .state('tab.newappointment', {
                url: '/newappointment',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/newappointment.html',
                        controller: 'NewAppointmentCtrl'
                    }
                }
            }).state('tab.itemdetail', {
                url: '/itemdetail/:item',
                cache: false,
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/tab-itemdetail.html',
                    }
                }
            }).state('tab.doctor', {
                url: '/doctor-listing',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/doctor-listing.html',
                        controller: 'DocListing'
                    }
                }
            }).state('tab.doctor-detail', {
                url: '/doctor-detail/:id',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/doctor-detail.html',
                        controller: 'DocDetail'
                    }
                }
            })

            .state('tab.myposts', {
                url: '/myposts',
                cache: false,
                views: {
                    'tab-myposts': {
                        templateUrl: 'templates/tab-myposts.html',
                        controller: 'MyPostsCtrl'
                    }
                }
            })
            .state('tab.insurancecompanies', {
                url: '/insurancecompanies',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/insurance-companies.html',
                        controller: 'InsuranceCompaniesCtrl'
                    }
                }
            })
            .state('tab.contactus',{
                url: '/contactus',
                cache: false,
                views: {
                    'st': {
                        templateUrl: 'templates/contactus.html',
                        controller: 'ContactUsCtrl'
                    }
                }
            })



        $urlRouterProvider.otherwise('/start/login');


    })
    .config(function ($translateProvider) {
        $translateProvider.translations('en', {
            BOOK_NOW: 'Book Now',
            MY_APPOINTMENTS: 'My appointments',
            ACCOUNT: 'Account',
            SIGN_OUT: 'Sign Out',
            BOOK_LATER: 'Book Later',
            BOOK_SPECIFIC_DOCTOR: 'Book Specific Doctor',
            SELECT_SPECIALTY: 'Select Specialty',
            SELECT_SUB_SPECIALTY: 'Select Sub Specialty',
            HOW_WILL_YOU_PAY: 'How will you pay?',
            DOCTOR_LIST: 'Doctor List',
            SORT: 'Sort',
            FILTER: 'Filter',
            DISTANCE: 'Distance',
            SPECIALITY: 'Speciality',
            CANCEL: 'Cancel',
            SORT_RESULT_BY: 'Sort Results By',
            FILTER_RESULTS_BY: 'Filter Results By',
            PAYMENT_METHOD: 'Payment Method',
            DOCTOR_DETAIL: 'Doctor Detail',
            ADD_A_REVIEW: 'Add a Review',
            REVIEW_TO_DOCTOR: 'Review To Doctor',
            SUBMIT: 'Submit',
            DOCTOR_DETAIL: 'Doctor Detail',
            SELECT_LANGUAGE: 'Select Language',
            INSURANCE_COMPANIES: 'Insurance Companies',
            ENGLISH: 'English',
            ARABIC: 'Arabic',
            GO: 'Go',
            LOGIN: 'Login',
            FORGOT_PASSWORD: 'Forgot Password',
            REGISTER_NEW_ACCOUNT: 'Register New Account',
            BACK: 'Back',
            ARE_YOU_SURE_TO_BOOK: 'Are you sure you want to book this appointment?',
            BOOK_APPOINTMENT: 'Book Appointment',
            CHECK_YOUR_APPOINTMENT: 'Check your Appointment',
            APPOINTMENT_SUCCESSFUL: 'Appointment Successful',
            YOUR_APPOINTMENT_NUMBER_IS: 'Your appointment number is',
            ACTION_FAILED: 'Action Failed',
            THANK_YOU_MOBILE_VERIFY: 'Thank you, mobile has been verified',
            WRONG_VERIFICATION_CODE: 'Wrong verification code',
            SUCCESSFULLY_SEND_CODE: 'Successfully send confirmation code',
            PLEASE_ENTER_VERIFICATION: 'Please enter verification code',
            RESEND_CODE: 'Resend Code',
            FIRST_NAME: 'First Name',
            LAST_NAME: 'Last Name',
            EMAIL: 'Email',
            GENDER: 'Gender',
            MALE: 'Male',
            FEMALE: 'Female',
            PASSWORD: 'Password',
            MOBILE: 'Mobile',
            CONFIRM_PASSWORD: 'Confirm Password',
            SORT_DISTANCE_HIGH: 'Sort by distance (high)',
            SORT_DISTANCE_LOW: 'Sort by distance (low)',
            SORT_RATING_LOW: 'Sort by rating (low)',
            SORT_RATING_HIGH: 'Sort by rating (high)',
            NO_RECORD_FOUND: 'No records found',
            January: 'January',
            February: 'February',
            March: 'March',
            April: 'April',
            May: 'May',
            June: 'June',
            July: 'July',
            August: 'August',
            September: 'September',
            October: 'October',
            November: 'November',
            December: 'December',
            Sunday: 'Sunday',
            Monday: 'Monday',
            Tuesday: 'Tuesday',
            Wednesday: 'Wednesday',
            Thursday: 'Thursday',
            Friday: 'Friday',
            Saturday: 'Saturday',
            Sun: 'Sun',
            Mon: 'Mon',
            Tue: 'Tue',
            Wed: 'Wed',
            Thu: 'Thu',
            Fri: 'Fri',
            Sat: 'Sat',
            CONTACT_US: 'Contact Us',
            MY_ACCOUNT: 'My Account',
            CHANGE_PASSWORD: 'Change Password',
            PATIENT_INFORMATION: 'Patient information',
            DOB: 'DOB',
            BLOOD_GROUP: 'Blood Group',
            GENDER: 'Gender',
            UPDATE: 'Update',
            YOUR_NAME: 'Your Name',
            MESSAGE: 'Message',
            SEND: 'Send',
            Search_By_Name:'Search By Name'
            


        });
        $translateProvider.translations('ar', {
            BOOK_NOW: 'احجز الآن',
            MY_APPOINTMENTS: 'مواعيدي',
            ACCOUNT: 'حسابي',
            SIGN_OUT: 'تسجيل خروج',
            BOOK_LATER: 'احجز بتاريخ معين',
            BOOK_SPECIFIC_DOCTOR: 'احجز حسب اسم الدكتور',
            SELECT_SPECIALTY: 'اختر التخصص',
            SELECT_SUB_SPECIALTY: 'اختر سبب الزيارة',
            HOW_WILL_YOU_PAY: 'كيف ستقوم بالدفع؟',
            DOCTOR_LIST: 'قائمة الأطباء',
            SORT: 'ترتيب',
            FILTER: 'فرز',
            DISTANCE: 'المسافة',
            SPECIALITY: 'التخصص',
            CANCEL: 'إلغاء',
            SORT_RESULT_BY: 'ترتيب النتائج حسب',
            FILTER_RESULTS_BY: 'تصفية النتائج حسب',
            PAYMENT_METHOD: 'طريقة الدفع',
            DOCTOR_DETAIL: 'معلومات الطبيب',
            ADD_A_REVIEW: 'تقييم',
            REVIEW_TO_DOCTOR: 'تقييم الطبيب',
            SUBMIT: 'إرسال',
            DOCTOR_DETAIL: 'معلومات الطبيب',
            SELECT_LANGUAGE: 'إختر اللغة',
            INSURANCE_COMPANIES: 'شركات التأمين',
            ENGLISH: 'إنجليزي',
            ARABIC: 'عربي',
            GO: 'استمر',
            LOGIN: 'تسجيل الدخول',
            FORGOT_PASSWORD: 'هل نسيت كلمة المرور',
            REGISTER_NEW_ACCOUNT: 'تسجيل حساب جديد',
            BACK: 'الى الخلف',
            ARE_YOU_SURE_TO_BOOK: 'هل أنت متأكد من حجز هذا الموعد؟',
            BOOK_APPOINTMENT: 'حجز موعد',
            CHECK_YOUR_APPOINTMENT: 'شاهد موعدك',
            APPOINTMENT_SUCCESSFUL: 'تم حجز الموعد بنجاح',
            YOUR_APPOINTMENT_NUMBER_IS: 'رقم حجزك هو',
            ACTION_FAILED: 'حدث خطأ',
            THANK_YOU_MOBILE_VERIFY: 'شكراً تم توثيق جوالك',
            WRONG_VERIFICATION_CODE: 'خطأ في رمز التحقق',
            SUCCESSFULLY_SEND_CODE: 'تم ارسال رمز التحقق بنجاح',
            PLEASE_ENTER_VERIFICATION: 'الرجاء ادخال رمز التحقق',
            RESEND_CODE: 'إعادة إرسال',
            FIRST_NAME: 'الاسم الأول',
            LAST_NAME: 'اسم العائلة',
            EMAIL: 'البريد الالكتروني',
            GENDER: 'الجنس',
            MALE: 'ذكر',
            FEMALE: 'أنثى',
            PASSWORD: 'كلمة المرور',
            MOBILE: 'التليفون المحمول',
            CONFIRM_PASSWORD: 'تأكيد كلمة المرور',
            SORT_DISTANCE_HIGH: '(فرز حسب المسافة (الأبعد',
            SORT_DISTANCE_LOW: '(فرز حسب المسافة (الأقرب',
            SORT_RATING_LOW: '(فرز حسب التقييم (الأقل',
            SORT_RATING_HIGH: '(فرز حسب التقييم (الأعلى',
            NO_RECORD_FOUND: 'لا توجد نتائج',
            January: 'كانون الثاني',
            February: 'شهر فبراير',
            March: 'مارس',
            April: 'أبريل',
            May: 'قد',
            June: 'يونيو',
            July: 'يوليو',
            August: 'أغسطس',
            September: 'سبتمبر',
            October: 'شهر اكتوبر',
            November: 'شهر نوفمبر',
            December: 'ديسمبر',
            Jan: 'كانون الثاني',
            Feb: 'شهر فبراير',
            Mar: 'مارس',
            Apr: 'أبريل',
            May: 'قد',
            Jun: 'يونيو',
            Jul: 'يوليو',
            Aug: 'أغسطس',
            Sep: 'سبتمبر',
            Oct: 'شهر اكتوبر',
            Nov: 'شهر نوفمبر',
            Dec: 'ديسمبر',
            Sunday: 'الأحد',
            Monday: 'الإثنين',
            Tuesday: 'الثلاثاء',
            Wednesday: 'الأربعاء',
            Thursday: 'الخميس',
            Friday: 'الجمعة',
            Saturday: 'السبت',
            Sun: 'الأحد',
            Mon: 'الإثنين',
            Tue: 'الثلاثاء',
            Wed: 'الأربعاء',
            Thu: 'الخميس',
            Fri: 'الجمعة',
            Sat: 'السبت',
            0: '۰',
            '01': '۱',
            '02': '۲',
            '03': '۳',
            '04': '٤',
            '05': '۵',
            '06': '٦',
            '07': '۷',
            '08': '۸',
            '09': '۹',
            1: '۱',
            2: '۲',
            3: '۳',
            4: '٤',
            5: '۵',
            6: '٦',
            7: '۷',
            8: '۸',
            9: '۹',
            10: '۱۰',
            11: '۱۱',
            12: '۱۲',
            13: '۱۳',
            14: '۱٤',
            15: '۱۵',
            16: '۱٦',
            17: '۱۷',
            18: '۱۸',
            19: '۱۹',
            20: '۲۰',
            21: '۲۱',
            22: '۲۲',
            23: '۲۳',
            24: '۲٤',
            25: '۲۵',
            26: '۲٦',
            27: '۲۷',
            28: '۲۸',
            29: '۲۹',
            30: '۳۰',
            31: '۳۱',
            2017: '۲۰۱۷',
            2018: '۲۰۱۸',
            2019: '۲۰۱۹',
            2020: '۲۰۲۰',
            CONTACT_US: 'اتصل بنا',
            MY_ACCOUNT: 'حسابي',
            USER_INFORMATION: 'معلومات المستخدم',
            CHANGE_PASSWORD: 'تغيير كلمة السر',
            PATIENT_INFORMATION: 'معلومات المريض',
            DOB: 'تاريخ الميلاد',
            BLOOD_GROUP: 'فصيلة الدم',
            GENDER: 'جنس',
            UPDATE: 'تحديث',
            YOUR_NAME: 'اسمك',
            MESSAGE: 'رسالة',
            SEND: 'إرسال',
            Search_By_Name:'البحث عن طريق الإسم'
            
            
        });
        var _language = localStorage.getItem('mwaeed:language') || 'en';
        //$rootScope.language = _language;
        $translateProvider.preferredLanguage(_language);
    });