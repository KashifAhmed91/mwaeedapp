var root = {};
var user = {};
// user = { "id": "8", "name": "Test user", "email": "test@test.com",
// "created_at": "2016-09-21 02:19:05", "updated_at": "2016-09-23 19:55:18",
// "is_admin": "1", "facebook_token": "facebooktokenisthisoneandonly", "image":
// "test.jpg", "distance": "10", "lat": "55.451784", "long": "10.385145",
// "message": "success" };
angular
    .module('starter.controllers', [])
    .controller('TabCtrl', function ($scope, CC, $rootScope, $ionicModal, $state, $ionicActionSheet, $translate, $ionicSideMenuDelegate) {
        var _user = JSON.parse(localStorage.getItem('user'));
        $scope.user = _user;
        console.log('---- Tab CTRL USER ---', _user);



        $scope.logout = function () {

            localStorage.removeItem("user");
            localStorage.removeItem("Token");

            $rootScope.user = {};

            CC.gotopage("start.login");

        }
        $scope.selectLanguage = function () {
            var hideSheet = $ionicActionSheet.show({
                buttons: [
                    { text: $translate.instant('ENGLISH') },
                    { text: $translate.instant('ARABIC') }

                ],
                titleText: $translate.instant('SELECT_LANGUAGE'),
                cancelText: $translate.instant('CANCEL'),
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    console.log(index);
                    var _languages = ['en', 'ar'],
                        _selectedLanguage = _languages[index];

                    if (_selectedLanguage) {
                        $translate.use(_selectedLanguage);
                        $rootScope.language = _selectedLanguage;
                        localStorage.setItem('mwaeed:language', _selectedLanguage);
                    }
                    return true;
                }
            });

        }
        $rootScope.$on('updateAccount', function () {
            var _user = JSON.parse(localStorage.getItem('user'));
            $scope.user = _user;
        });
        $scope.toggleLeft = function () {
            console.log('!!!!!');
            $ionicSideMenuDelegate.toggleLeft();
        };

    })
    .controller('ForgotCtrl', function ($scope, CC) {

        $scope.login_form = {
            "email": ""
        };

        $scope.myGoBack = function () {
            $ionicHistory.goBack();
        };

        $scope.forgotPassword = function () {
            username = $.trim(this.login_form.email);
            if (username == '') {
                CC.msg("Email is required");
                return false;
            }

            url = "reset-password";
            sdata = {
                "email": username
            };

            CC.postform(url, sdata, function (data) {

                if (data[0].code == "200") {

                    user = data[0].user;
                    CC.gotopage("tab.dash");

                } else {
                    CC.msg(data[0].message);
                    return false;
                }

            });
        }

    })
    .controller('ChangePasswordCtrl', function ($scope, CC, $rootScope) {

        $scope.frm = {
            "existingPassword": "",
            "newPassword": "",
            "confirmPassword": ""
        };

        $scope.myGoBack = function () {
            $ionicHistory.goBack();
        };

        $scope.forgotPassword = function () {

            existingPassword = $.trim(this.frm.existingPassword);
            newPassword = $.trim(this.frm.newPassword);
            confirmPassword = $.trim(this.frm.confirmPassword);
            console.log($rootScope.user);

            if (existingPassword != $rootScope.user.password) {
                CC.msg("Current Password is not correct");
                return false;
            }

            if (newPassword == "") {
                CC.msg("Please enter new Password");
                return false;
            }
            if (confirmPassword != newPassword) {
                CC.msg("Password do not match");
                return false;
            }

            url = "update-password";
            sdata = {
                "api_token": localStorage.Token,
                "password": newPassword,
                "password_confirmation": confirmPassword
            };

            CC.postform(url, sdata, function (data) {

                if (data[0].code == "200") {

                    user = data[0].user;
                    CC.gotopage("tab.dash");

                } else {
                    CC.msg(data[0].message);
                    return false;
                }

            });
        }

    })
    .controller('LoginCtrl', function ($scope, CC, $rootScope, $window, $http, $ionicHistory, $state, $translate) {

        var oneSignal = {};
        if (localStorage.user) {
            try {
                user = JSON.parse(localStorage.user);
                $rootScope.user = user;
                CC.gotopage("tab.dash");

            } catch (err) { }
        }

        $scope.login_form = {
            "username": "",
            "password": ""
        };
        // $scope.login_form = {
        //     "username": "mineb@dnsdeer.com",
        //     "password": "shebkhan"
        // };

        $scope.doLogin = function () {
            username = $.trim(this.login_form.username);
            password = $.trim(this.login_form.password);
            if (username == '') {
                CC.msg("Username is required");
                return false;
            }
            if (password == '') {
                CC.msg("Password is required");
                return false;
            }

            url = "login";
            sdata = {
                "email": username,
                "password": password,
                "api_token": "",
                 "device_os": "iOS",
                 "platform": "iOS",
                //"device_os": "android",
                //"platform": "android"
            };
            if (window && window.device && window.device.uuid) {
                sdata.device_id = window.device.uuid;
            }

            //CC.msg(JSON.stringify(sdata));
            getOneSignalDeviceId(function (oneSignal) {
                sdata.onesignal_ios_device_id = oneSignal.device_id;
                sdata.onesignal_ios_pushToken = oneSignal.pushToken;
                sdata.onesignal_android_id = oneSignal.device_id;
                sdata.onesignal_android_pushToken = oneSignal.pushToken;
                //CC.msg(JSON.stringify(sdata));
                CC.postform(url, sdata, function (data) {
                    console.log(data);
                    if (data[0]) {
                        if (data[0].code == '200' || (data[0].code == '206' && data[0].user_object && data[0].user_object.mobile_verified == '1')) {
                            api_token = data[0].api_token;
                            localStorage.Token = api_token;
                            user = data[0].user || data[0].user_object;
                            user.password = password;
                            $rootScope.user = user;
                            localStorage.user = JSON.stringify(user);
                            CC.gotopage("tab.dash");
                        } else if (data[0].code == '407' && data[0].user.email) {
                            $state.go("start.confirmationaccount", { email: data[0].user.email });
                        } else if (data[0].code == '206' && data[0].user_object && data[0].user_object.mobile_verified == '0') {

                            $state.go("start.confirmationaccount", {
                                email: data[0].user_object.email,
                                password: password
                            });
                        }
                        else {
                            CC.msg(data[0].message);
                            return false;
                        }
                    }
                }, function (err) {
                    debugger;
                    if (err.error == 'Your account not confirmed yet') {
                        $state.go("start.confirmationaccount", { email: username });
                    }
                });
            });

        }

        $scope.myGoBack = function () {
            $ionicHistory.goBack();
        };

        $scope.form = {
            "first_name": "",
            "last_name": "",
            "mobile": "",
            "email": "",
            "password": "",
            "password_confirmation": "",
            "gender": "0"

        }

        $scope.disbalesubmit = false;
        $scope.registerAccount = function () {

            if ($.trim($scope.form.first_name) == "") {
                CC.msg("Please enter First Name");
                return false;
            }
            if ($.trim($scope.form.last_name) == "") {
                CC.msg("Please enter Last Name");
                return false;
            }
            if ($.trim($scope.form.mobile) == "") {
                CC.msg("Please enter your Mobile Number");
                return false;
            }

            if ($.trim($scope.form.email) == "") {
                CC.msg("Please enter your Email");
                return false;
            }
            if ($.trim($scope.form.password) == "") {
                CC.msg("Please enter your Password");
                return false;
            }
            if ($.trim($scope.form.gender) == "") {
                CC.msg("Please enter your Gender");
                return false;
            }


            $scope.disbalesubmit = true;

            CC.showloader();

            url = "register";
            if ($scope.form.mobile && $scope.form.mobile[0] != '+') {
                $scope.form.mobile = '+' + $scope.form.mobile;
            }
            sdata = {
                "first_name": $scope.form.first_name,
                "last_name": $scope.form.last_name,
                "mobile": $scope.form.mobile,
                "email": $scope.form.email,
                "password": $scope.form.password,
                "password_confirmation": $scope.form.password_confirmation,
                "gender": $scope.form.gender,
                // "device_os": "iOS",
                // "platform": "iOS"
                "device_os": "android",
                "platform": "android"
            }

            getOneSignalDeviceId(function (oneSignal) {
                sdata.onesignal_ios_device_id = oneSignal.device_id;
                sdata.onesignal_ios_pushToken = oneSignal.pushToken;
                sdata.onesignal_android_id = oneSignal.device_id;
                sdata.onesignal_android_pushToken = oneSignal.pushToken;
                //CC.alert(JSON.stringify(sdata));
                CC.postform(url, sdata, function (data) {
                    if (data[0].code == "200") {
                        localStorage.Token = data[0].api_token;
                        CC.msg("You need to confirm your account");
                        $state.go("start.confirmationaccount", {
                            email: data[0].email,
                            password: $scope.form.password
                        });
                    } else {
                        msg = "";
                        if (data[0].message instanceof Object) {
                            $.each(data[0].message, function (index, el) {
                                msg = msg + el[0] + "\n";
                            });
                        } else {
                            console.log('Register Account Error Message: ', data)
                            msg = data[0].message || "Application Error";
                        }
                        CC.msg(msg);
                        $scope.disbalesubmit = false;
                        return false;
                    }
                    $scope.disbalesubmit = false;
                });
            });
            CC.hideloader();

        }

        $scope.switchLanguage = function () {
            var _selectedLanguage = $rootScope.language == 'en' ? 'ar' : 'en';
            $translate.use(_selectedLanguage);
            $rootScope.language = _selectedLanguage;
            localStorage.setItem('mwaeed:language', _selectedLanguage);
        }

        function getOneSignalDeviceId(callBack) {
            if (window.plugins && window.plugins.OneSignal) {
                window.plugins.OneSignal.getIds(function (d) {
                    callBack({
                        device_id: d.userId,
                        pushToken: d.pushToken
                    });
                });
            } else {
                callBack({
                    device_id: null,
                    pushToken: null
                });
            }
        }

    })
    .controller('DashCtrl', function ($scope, CC, $ionicHistory, $ionicSideMenuDelegate, $state) {

        $scope.$on('$ionicView.loaded', function () {
            var posOptions = { timeout: 10000, enableHighAccuracy: false };
        });
        $scope.doclist = function () {
            $ionicHistory.nextViewOptions({ disableBack: true });
            CC.gotopage("tab.doctor");
        }
        $scope.book_now = function(){
            var _date_ = new Date(),
                _selectedDate = {
                    year: _date_.getFullYear(),
                    month: _date_.getMonth()+1,
                    day: _date_.getDay()
                };
            if(_selectedDate.month<10){
                _selectedDate.month = "0"+_selectedDate.month
            }
            $state.go('tab.speciality', {
                date: _selectedDate.year + '' + _selectedDate.month + '' + _selectedDate.day
            });
            
        }
        
        $scope.toggleMenu = function () {
            $ionicSideMenuDelegate.toggleLeft();
        };

    })
    .controller('AccountCtrl', function ($scope, CC, $ionicPopup, $rootScope, $ionicSideMenuDelegate) {

        var _user = JSON.parse(localStorage.getItem('user'));
        console.log("User:", _user);
        _user.patient = typeof _user.patient == 'string' ? JSON.parse(_user.patient) : _user.patient;
        _user.blod_type = _user.patient.blod_type || null;
        _user.gender = _user.patient.gender || null;
        console.log(_user);

        $scope.accountTheme = {
            gender: false,
            blod_type: false
        }
        $scope.$on('$ionicView.loaded', function () { });
        $scope.changePassword = function () {
            CC.gotopage("tab.changepassword");
        }
        $scope.user = _user;
        $scope.bloodGroupArray = [
            {
                "id": 1,
                "value": "A+"
            }, {
                "id": 2,
                "value": "A-"
            }, {
                "id": 3,
                "value": "B+"
            }, {
                "id": 4,
                "value": "B-"
            }, {
                "id": 5,
                "value": "AB+"
            }, {
                "id": 6,
                "value": "AB-"
            }, {
                "id": 7,
                "value": "O+"
            }, {
                "id": 8,
                "value": "O-"
            }
        ];
        $scope.toggle = function (type) {
            console.log(type, $scope.accountTheme[type]);
            $scope.accountTheme[type] = !$scope.accountTheme[type];
        }

        $scope.updateMyAccount = function () {
            var queryObject = angular.copy($scope.user),
                _token = localStorage.getItem('Token'),
                url = 'update-my-account?api_token=' + _token;
            queryObject.patient = JSON.stringify(queryObject.patient);

            CC.postform(url, queryObject, function (_data) {
                console.log(_data)
                if (_data.code == "200") {
                    var queryObject = angular.copy($scope.user);
                    queryObject.patient = JSON.stringify(queryObject.patient);
                    localStorage.setItem('user', JSON.stringify(queryObject));
                    //CC.msg("Your account successfully Updated");
                    $rootScope.$emit('updateAccount');
                    var alertPopup = $ionicPopup.alert({ title: 'Account Updated', template: 'Your Account Successfully Updated!' });

                    alertPopup.then(function (res) {
                        console.log('Account Updated!!');
                    });
                    //CC.gotopage("tab.dash");
                } else {
                    msg = "";
                    if (_data[0].message instanceof Object) {
                        $
                            .each(_data.message, function (index, el) {
                                msg = msg + el[0] + "\n";

                            });
                    }
                    CC.msg(msg);
                    $scope.disbalesubmit = false;
                    return false;
                }
                $scope.disbalesubmit = false;
            });
        }

        $scope.toggleLeft = function () {
            $ionicSideMenuDelegate.toggleLeft();
        };

    })
    .controller('SpecialityCtrl', function ($scope, CC, $stateParams, $rootScope, $state, $ionicHistory) {

        $scope.notasks = false;
        console.log($stateParams);
        if ($stateParams.date.length > 0) {
            $rootScope.doctorSpecificDate = $stateParams.date;
        } else {
            $rootScope.doctorSpecificDate = null;
        }
        $scope.getData = function () {
            url = "specialties";
            params = {
                "api_token": localStorage.Token
            };

            CC.calljson(url, params, function (data) {

                if (data.data.length > 0) {
                    $scope.data = data.data;

                } else { }

            });
        }

        $scope.back = function () {
            console.log("!!! BACK !!!");
            $ionicHistory.goBack();
        }

        $scope.$on('$ionicView.loaded', function () {
            $scope.getData();
        });
    })
    .controller('PaymentCtrl', function ($scope, CC, $rootScope, $ionicHistory) {

        $scope.notasks = false;

        $scope.getData = function () {
            url = "payment-methods";
            params = {
                "api_token": localStorage.Token
            };

            CC.calljson(url, params, function (data) {

                if (data.data.length > 0) {
                    $scope.data = data.data;

                } else { }

            });
        }

        $scope.$on('$ionicView.loaded', function () {

            $scope.getData();

        });

        $scope.showList = function (id) {
            $rootScope.Paymentmethod = id;
            if (id == 2) {
                CC.gotopage('tab.insurancecompanies');
            } else {
                $rootScope.insurance = null;
                CC.gotopage("tab.doctor");
            }

        }
        $scope.back = function () {
            $ionicHistory.goBack();
        }

    })
    .controller('SubSpecialityCtrl', function ($scope, CC, $stateParams, $rootScope, $ionicHistory) {

        $scope.notasks = false;
        id = $stateParams.id;
        $rootScope.Speciality = id;
        debugger;
        $scope.getData = function () {
            debugger;
            url = "specialties/" + id;
            params = {
                "api_token": localStorage.Token
            };

            CC.calljson(url, params, function (data) {

                if (data.data.length > 0) {
                    $scope.data = data.data;

                } else {
                    CC.gotopage("tab.pay");
                }

            });
        }

        $scope.$on('$ionicView.loaded', function () {

            $scope.getData();

        });

        $scope.gotopaymentPage = function (id) {
            $rootScope.Subspeciality = id;
            CC.gotopage("tab.pay");
        }
        $scope.back = function () {
            $ionicHistory.goBack();
        }

    })
    .controller('CalendarCtrl', function ($scope, CC, $filter, $rootScope, $state, $ionicPopup, $ionicHistory) {

        $scope
            .$on('$ionicView.loaded', function () { });

        if ($rootScope.calenderDefaultDate) {
            var calenderDefaultDate = $rootScope.calenderDefaultDate.year + "-" + $rootScope.calenderDefaultDate.month + "-" + $rootScope.calenderDefaultDate.day;
            console.log(calenderDefaultDate);
            //$scope.selectedMonth = 
        } else {
            var calenderDefaultDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            console.log('default', calenderDefaultDate);
        }

        $scope.dt = {
            day: $filter('date')(new Date(calenderDefaultDate), 'dd'),
            month: $filter('date')(new Date(calenderDefaultDate), 'MMM'),
            year: $filter('date')(new Date(calenderDefaultDate), ' yyyy')
        };
        $scope.day = $filter('date')(new Date(calenderDefaultDate), 'EEEE');
        $scope.month = $filter('date')(new Date(calenderDefaultDate), 'LLLL yyyy');

        //var calenderDefaultDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        //var calenderDefaultDate = $rootScope.calenderDefaultDate || $filter('date')(new Date(), 'yyyy-MM-dd');
        var cdt = new Date(),
            yesterday = new Date(cdt.getTime() - 24 * 60 * 60 * 1000);
        $scope.calendarOptions = {
            defaultDate: calenderDefaultDate,
            minDate: yesterday,
            maxDate: new Date([2020, 12, 31]),
            dayNamesLength: 3, // How to display weekdays (1 for "M", 2 for "Mo", 3 for "Mon"; 9 will show full day names; default is 1)
            multiEventDates: false, // Set the calendar to render multiple events in the same day or only one event, default is false
            maxEventsPerDay: 0, // Set how many events should the calendar display before showing the 'More Events' message, default is 3;
            eventClick: function (dt) {
                console.log(dt);

                //  $rootScope.selectedDate = dt.day + "/" + dt.month + "/" + dt.year;
                //  console.log($rootScope.selectedDate); CC.gotopage("tab.doctor");
            },
            dateClick: function (dt) {
                //$rootScope.doctorDate = dt.day + "/" + dt.month + "/" + dt.year;
                console.log(dt);
                if (dt && dt.day && dt.month && dt.year) {
                    dt.month++;

                    $rootScope.calenderDefaultDate = dt;
                    if (dt.month < 10) {
                        dt.month = '0'+dt.month;
                    }
                    if(dt.day < 10){
                        dt.day = '0'+dt.day;
                    }
                    $scope.calendarOptions.defaultDate = dt.year + '-' + dt.month + '-' + dt.day;
                    // $scope.dt = $filter('date')(new Date($scope.calendarOptions.defaultDate), 'MMM') + " <span> " + $filter('date')(new Date($scope.calendarOptions.defaultDate), 'dd') + " </span> " + $filter('date')(new Date($scope.calendarOptions.defaultDate), ' yyyy');

                    $scope.dt = {
                        day: $filter('date')(new Date($scope.calendarOptions.defaultDate), 'dd'),
                        month: $filter('date')(new Date($scope.calendarOptions.defaultDate), 'MMM'),
                        year: $filter('date')(new Date($scope.calendarOptions.defaultDate), ' yyyy')
                    };
                    $scope.day = $filter('date')(new Date($scope.calendarOptions.defaultDate), 'EEEE');
                    $scope.month = $filter('date')(new Date($scope.calendarOptions.defaultDate), 'LLLL yyyy');
                }
            }
        };

        $scope.setCalenderDate = function () {
            var _calenderDoctorDate = $rootScope.calenderDefaultDate.year + '' + $rootScope.calenderDefaultDate.month + '' + $rootScope.calenderDefaultDate.day;
            //$rootScope.calenderDefaultDate = dt;
            $state.go('tab.speciality', {
                date: _calenderDoctorDate
            });
        }
        $scope.back = function () {
            $ionicHistory.goBack();
        }
    })
    .controller('NewAppointmentCtrl', function ($scope, CC, $rootScope, $ionicPopup, $ionicHistory, $filter, $translate, $ionicSideMenuDelegate) {

        $scope.data = $rootScope.appointedDoctor;
        debugger;
        $scope.timeslot = $scope.data.time_slots[$rootScope.DoctorAppointmentTimeSlot];
        $scope.appontmentNumber = null;
        $ionicSideMenuDelegate.canDragContent(true)
        console.log($scope.data);
        console.log($scope.timeslot);
        $scope.$on('$ionicView.loaded', function () { });

        $scope.saveAppointment = function () {
            var dt = $scope.timeslot.date + ' ' + $scope.timeslot.time_from,
                message_template;
            time = $filter('date')(moment(dt, "YYYY-MM-DD HH:mm A").toString(), 'HH:mm:ss');
            dt = $filter('date')(new Date($scope.timeslot.date), 'dd-MM-yyyy');
            if ($rootScope.language == 'en') {
                message_template = "<div>" + $translate.instant('ARE_YOU_SURE_TO_BOOK') + "</div>"
            } else {
                message_template = "<div style='width:100%; text-align: right'>" + $translate.instant('ARE_YOU_SURE_TO_BOOK') + "</div>"
            }
            var confirmPopup = $ionicPopup.confirm({
                title: $translate.instant('BOOK_APPOINTMENT'),
                template: message_template
            }),
                _user = JSON.parse(localStorage.getItem('user'));

            confirmPopup.then(function (res) {
                if (res) {

                    params = {
                        "appointment_date": dt,
                        "time": time,
                        "doctor": $scope.data.id,
                        "patient": _user.id
                    };
                    if ($rootScope.Subspeciality) {
                        params['sub_specialty'] = $rootScope.Subspeciality;
                    }
                    if ($rootScope.Speciality) {
                        params['specialty'] = $rootScope.Speciality;
                    }
                    if ($rootScope.Paymentmethod) {
                        params['payment_method'] = $rootScope.Paymentmethod;
                    }
                    url = "book-appointment/?api_token=" + localStorage.Token;
                    console.log(url);
                    CC.postform(url, params, function (response) {
                        console.log('Appointment Book', response);
                        if (response.code == 200) {
                            $('.nav-bar-block').css('display', 'none');
                            $scope.appontmentNumber = response.data;
                            setTimeout(function () {
                                $('.nav-bar-block').css('display', 'block');
                                $rootScope.appointedDoctor = {};
                                $rootScope.timeslot = {};
                                $ionicHistory.nextViewOptions({ disableBack: true });
                                $scope.appontmentNumber = null;
                                CC.gotopage("tab.dash");
                            }, 3000);
                            // $ionicPopup.alert({     title: 'Appointment Successful',     template: "You
                            // appointment number is: #" + response.data }).then(function () { });
                        } else {
                            var errorAlert = $ionicPopup.alert({ title: $translate.instant('ACTION_FAILED'), template: response.message });
                        }

                    });

                } else {
                    console.log('You are not sure');

                    return false;
                }
            });

        }
        $scope.back = function () {
           $ionicHistory.goBack();
        }

    })
    .controller('MyAppointmentCtrl', function ($scope, CC, $ionicSideMenuDelegate, $rootScope) {
        $scope.data = [];
        $scope.getData = function () {
            var _user = JSON.parse(localStorage.getItem('user'));
            console.log(_user);
            if (_user.id) {
                var url = "appointments/",
                    params = {
                        "api_token": localStorage.Token
                    },
                    uid = _user.id;

                CC.calljson(url + uid, params, function (data) {
                    if (data.length > 0) {
                        $scope.data = data;
                        console.log('My Appointment List', data);
                    }
                });
            } else {
                CC.gotopage('start.login');
            }

        }
        $scope.toggleLeft = function () {
            $ionicSideMenuDelegate.toggleLeft();
        };
        $rootScope.$on('notificationOpened', function() { 
            console.log('notificationOpened');
            $scope.getData(); 
        });
        $scope.$on('$ionicView.loaded', function () {
            console.log('----- My Appointment Controller Loaded -----');
            $scope.getData();
        });
        $scope.doRefresh = function () {
            setTimeout(function () {
                $scope.getData();
                $scope.$broadcast('scroll.refreshComplete');
            }, 1000)

        };
    })
    .controller('SearchCtrl', function ($scope, CC, $rootScope, $ionicHistory) {
        $scope.frm = {
            "clinicName": "",
            "doctorName": ""
        };

        $scope.searchDoctors = function () {

            clinicName = this.frm.clinicName;
            doctorName = this.frm.doctorName;

            // if (clinicName == '' && doctorName == '') {     CC.msg("Please enter any one
            // criteria");     return false; }
            if (clinicName != "") {

                $rootScope.clinicName = clinicName;

            }
            if (doctorName != "") {

                $rootScope.doctorName = doctorName;

            }
            CC.gotopage("tab.doctor");

        }

        $scope.$on('$ionicView.loaded', function () { });
        $scope.back = function () {
            $ionicHistory.goBack();
        }

    })
    .controller('DocListing', function ($scope, CC, $ionicModal, $state, $ionicListDelegate, $ionicActionSheet, $rootScope, $cordovaGeolocation, $translate, $ionicSideMenuDelegate, $ionicHistory) {

        $scope.notasks = false;
        speciality = $rootScope.Speciality;
        subspeciality = $rootScope.Subspeciality;
        $scope._orderBy = '-distance';
        $ionicSideMenuDelegate.canDragContent(true)
        $scope.getData = function () {
            url = "list-doctors";
            params = {
                "page": 1,
                "limit": 10,
                "rate": 0,
                "api_token": localStorage.Token
            };
            if (speciality != "") {
                params.specialty = speciality;
            }
            if ($rootScope.insurance) {
                params.insurance = $rootScope.insurance;
            }
            if ($rootScope.Paymentmethod) {
                params.payment_method = $rootScope.Paymentmethod;
            }
            if ($rootScope.Subspeciality) {

            }
            if ($scope.lat != "" && $scope.long != "") {
                params.user_lat = $scope.lat;
                params.user_lon = $scope.long;
            }
            if ($rootScope.doctorName) {
                params.name = $rootScope.doctorName;
            }
            if ($rootScope.doctorSpecificDate) {
                params.date = $rootScope.doctorSpecificDate;
            }
            CC.calljson(url, params, function (data) {
                if (data && typeof data != 'string' && data.length > 0) {
                    $scope.data = data;
                } else {
                    CC.msg($translate.instant('NO_RECORD_FOUND'))
                }
            });
        }

        $scope.docdetail = function (id) {
            CC.gotopage("tab.doctor-detail/" + id);
        }

        $scope.$on('$ionicView.loaded', function () {

            var posOptions = {
                timeout: 10000,
                enableHighAccuracy: false
            };
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {

                    console.log(position);
                    $scope.lat = position.coords.latitude;
                    $scope.long = position.coords.longitude;
                    $rootScope.position = {
                        lat: $scope.lat,
                        lng: $scope.long
                    };
                    $scope.getData();

                }, function (err) {
                    console.log("err",err);
                    //  $scope.getData();
                });

        });

        $scope.$on('$ionicView.unloaded', function (event, data) { });

        $scope.showsort = function () {

            $ionicActionSheet.show({
                titleText: $translate.instant('SORT_RESULT_BY'),
                buttons: [
                    {
                        text: $translate.instant('SORT_DISTANCE_HIGH')
                    },
                    {
                        text: $translate.instant('SORT_DISTANCE_LOW')
                    },
                    {
                        text: $translate.instant('SORT_RATING_LOW')
                    },
                    {
                        text: $translate.instant('SORT_RATING_HIGH')
                    }
                ],
                cancelText: $translate.instant('CANCEL'),
                cancel: function () { },
                buttonClicked: function (index) {
                    var orderBy = ['-distance', 'distance', 'rating', '-rating'];
                    $scope._orderBy = orderBy[index];
                    console.log('BUTTON CLICKED', index);
                    return true;
                }
            });
        };
        $scope.showfilter = function () {

            $ionicActionSheet.show({
                titleText: $translate.instant('FILTER_RESULTS_BY'),
                buttons: [
                    {
                        text: $translate.instant('SPECIALITY')
                    }, {
                        text: $translate.instant('PAYMENT_METHOD')
                    }
                ],
                cancelText: $translate.instant('CANCEL'),
                cancel: function () { },
                buttonClicked: function (index) {
                    console.log('BUTTON CLICKED', index);
                    return true;
                }
            });
        };
        $scope.back = function () {
            $ionicHistory.goBack();
        }

    })
    .controller('DocDetail', function ($scope, CC, $ionicModal, $state, $ionicListDelegate, $stateParams, $rootScope, $ionicPopup, $cordovaGeolocation,$ionicHistory, $ionicSideMenuDelegate) {

        $ionicSideMenuDelegate.canDragContent(false);
        id = $stateParams.id;
        $scope.notasks = false;
        $scope.docRating = { stars: 2, comment: '' };
        $scope.ratingsObject = {
            iconOn: 'ion-ios-star',
            iconOff: 'ion-ios-star-outline',
            iconOnColor: 'rgb(200, 200, 100)',
            iconOffColor: 'rgb(200, 100, 100)',
            rating: $scope.docRating.stars,
            minRating: 1,
            callback: function (rating) {
                console.log('--- Rating ---', rating);
                $scope.docRating.stars = rating;
            }
        };
        $scope.mapHeight = (window.innerHeight - 20) + 'px';

        $ionicModal.fromTemplateUrl('map-direction.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });

        var params = {
            "api_token": localStorage.Token,
            "doctor_id": id
        };

        if ($rootScope.position) {
            if ($rootScope.position.lat != "" && $rootScope.position.long != "") {
                params.user_lat = $rootScope.position.lat;
                params.user_lon = $rootScope.position.lng;
            }
        }
        if ($rootScope.doctorSpecificDate) {
            params.date = $rootScope.doctorSpecificDate;
        }
        $scope.getData = function () {
            url = "view-doctor";
            CC.calljson(url, params, function (data) {
                if (data) {
                    $scope.data = data;
                    $scope.position = {
                        position: [data.latitude, data.longitude]
                    };
                    $scope.opts = {
                        center: [data.latitude, data.longitude]
                    };
                }

            });
        }

        $scope.$on('$ionicView.loaded', function () {

            $scope.getData();

        });

        $scope.$on('$ionicView.unloaded', function (event, data) { });

        $scope.newappointment = function (ID) {
            $rootScope.DoctorAppointmentTimeSlot = ID;
            $rootScope.appointedDoctor = $scope.data;
            console.log($rootScope.appointedDoctor, $rootScope.DoctorAppointmentTimeSlot);
            //$rootScope.$digest($rootScope.appointedDoctor);
            CC.gotopage("tab.newappointment");

        }

        $scope.giveReview = function () {
            console.log(" --- Give Review popup --- ");
            var myPopup = $ionicPopup.show({
                template: "<ionic-ratings ratingsobj='ratingsObject'></ionic-ratings><textarea placeholder='Your Review to doctor ...' rows='8' cols='10' ng-model='docRating.comment'></textarea>",
                title: "'Review To Doctor",
                scope: $scope,
                buttons: [
                    {
                        text: 'Cancel'
                    }, {
                        text: '<b>Submit</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            var _token = localStorage.getItem('Token'),
                                _url = 'rate-doctor/' + id + '?api_token=' + _token;

                            CC.postform(_url, $scope.docRating, function (_response) {
                                console.log(_response);
                                $scope.getData();
                            })
                        }
                    }
                ]
            });
        }

        $scope.getDirection = function () {
            $scope.wayPoints = [];
            $scope.docLocation = new google.maps.LatLng($scope.data.latitude, $scope.data.longitude);
            $scope.wayPoints.push({
                location: $scope.docLocation,
                stopover: true
            });
            var posOptions = {
                timeout: 10000,
                enableHighAccuracy: false
            };
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {
                    $scope.myLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    $scope.wayPoints.push({
                        location: $scope.myLocation,
                        stopover: true
                    });

                }, function (err) {
                    console.log(err);
                });


            $scope.modal.show();
        }
        $scope.closeDirection = function(){
            $scope.myLocation = undefined;
            $scope.wayPoints = [];
            $scope.modal.hide();
        }
        $scope.back = function () {
            $ionicHistory.goBack();
        }

    })
    .controller('InsuranceCompaniesCtrl', function ($scope, CC, $rootScope) {
        $scope.data = [];

        $scope.getData = function () {
            var url = "insurance-companies",
                params = {
                    "api_token": localStorage.Token
                };

            CC.calljson(url, params, function (response) {
                if (response.data.length > 0) {
                    $scope.data = response.data;
                    console.log('--- InsuranceCompanies ---', response.data);
                }
            });
        }

        $scope.showList = function (id) {
            //$rootScope.Paymentmethod = id;
            $rootScope.insurance = id;
            CC.gotopage("tab.doctor");
        }

        $scope.$on('$ionicView.loaded', function () {
            console.log('----- InsuranceCompanies Controller Loaded -----');
            $scope.getData();
        });
    })
    .controller('confirmationAccount', function ($scope, $stateParams, CC, $translate, $rootScope) {
        $scope.confirmation_form = {
            email: $stateParams.email
        };
        console.log($stateParams);

        $scope.submitCode = function () {
            if ($scope.confirmation_form.code) {
                CC.postform('activate', $scope.confirmation_form, function (response) {
                    if (response[0] && response[0].code == '200') {
                        CC.msg($translate.instant('THANK_YOU_MOBILE_VERIFY'));

                        // Login & Navigate to Home Screen
                        getOneSignalDeviceId(function (oneSignal) {
                            var sdata = {
                                "email": $stateParams.email,
                                "password": $stateParams.password,
                                "api_token": "",
                                // "device_os": "iOS",
                                // "platform": "iOS"
                                "device_os": "android",
                                "platform": "android"
                            };
                            if (window && window.device && window.device.uuid) {
                                sdata.device_id = window.device.uuid;
                            }

                            // sdata.onesignal_ios_device_id = oneSignal.device_id;
                            // sdata.onesignal_ios_pushToken = oneSignal.pushToken;
                            sdata.onesignal_android_id = oneSignal.device_id;
                            sdata.onesignal_android_pushToken = oneSignal.pushToken;
                            //CC.msg(JSON.stringify(sdata));
                            CC.postform('login', sdata, function (data) {
                                console.log(data);
                                if (data[0]) {
                                    if (data[0].code == '206') {
                                        api_token = data[0].api_token;
                                        localStorage.Token = api_token;
                                        user = data[0].user_object;
                                        user.password = $stateParams.password;
                                        $rootScope.user = user;
                                        localStorage.user = JSON.stringify(user);
                                        CC.gotopage("tab.dash");
                                    } else {
                                        CC.msg(data[0].message);
                                        return false;
                                    }
                                }
                            }, function (err) {
                                if (err.error == 'Your account not confirmed yet') {
                                    $state.go("start.confirmationaccount", { email: username });
                                }
                            });
                        });
                    } else {
                        CC.msg($translate.instant('WRONG_VERIFICATION_CODE'));
                    }

                }, function (err) {
                    CC.msg(JSON.stringify(err));
                    console.log(err);
                })
            }
        }

        $scope.resendCode = function () {
            var _user = {
                email: $stateParams.email
            }
            CC.calljson('send-code', _user, function (response) {
                if (response[0].code == '200') {
                    CC.msg($translate.instant('SUCCESSFULLY_SEND_CODE'));
                } else if (response[0].code) {
                    var error_msg = response[0].message || $translate.instant('ACTION_FAILED');
                    CC.msg(error_msg);
                } else {
                    CC.msg($translate.instant('ACTION_FAILED'));
                }
            });
        }


        function getOneSignalDeviceId(callBack) {
            if (window.plugins && window.plugins.OneSignal) {
                window.plugins.OneSignal.getIds(function (d) {
                    callBack({
                        device_id: d.userId,
                        pushToken: d.pushToken
                    });
                });
            } else {
                callBack({
                    device_id: null,
                    pushToken: null
                });
            }
        }
    })
    .controller('ContactUsCtrl', function ($scope, CC, $translate) {

        $scope.contactus = {
            email: user.email,
            mobile: user.mobile,
            name: (user.first_name || '') + ' ' + (user.last_name || ''),
        }
        console.log($scope.contactus)
        $scope.submitContactus = function () {
            //console.log('ContactUS');
            CC.postform('contact ', $scope.contactus, function (data) {
                if (data[0]) {
                    if (data[0].code == '200') {
                        CC.msg($translate.instant('SEND'));
                        CC.gotopage("tab.dash");
                    } else {
                        CC.msg(data[0].message);
                        return false;
                    }
                } else {
                    CC.msg($translate.instant('ACTION_FAILED'));
                }
            }, function (err) {
                if (err && err.error) {
                    CC.msg(err.error);
                } else {
                    CC.msg($translate.instant('ACTION_FAILED'));
                }
            });
        }

        console.log('Contact US')
    })
    .factory('CC', function ($ionicLoading, $state, $rootScope, $http, $sce, $ionicActionSheet, $cordovaGeolocation, $cordovaToast, $ionicPopup) {

        root.apiurl = 'https://mwaeedtech.com/api/';
        root.mediaurl = '';
        root.fileuploadurl = '';
        $rootScope.mediaurl = root.mediaurl;
        root.renderURL = function (html_code) {
            return $sce.trustAsResourceUrl(html_code);
        };
        root.showloader = function (txt) {

            if (txt === void 0) {

                txt = "Loading..."
            }
            $ionicLoading.show({ template: '<ion-spinner icon="spiral"></ion-spinner>' });
        }
        root.hideloader = function () {
            $ionicLoading.hide();
        }
        root.msg = function (txt) {

            try {
                debugger;
                $cordovaToast
                    .showShortBottom(txt)
                    .then(function (success) {
                        console.log("--- $cordovaToast.showShortBottom: success ---", success);
                    }, function (error) {
                        console.log("--- $cordovaToast.showShortBottom: error ---", error);
                    });

            } catch (err) {

                alert(txt);

            }

        }
        root.isEmail = function (email) {
            return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(email);
        }
        root.calljson = function (url, sdata, callsucess) {

            hasconnection = false;
            if (window.cordova) {
                if (window.Connection) {
                    if (navigator.connection.type == Connection.NONE) {
                        hasconnection = false;
                        root.msg("Error establishing Internet Connection");
                        return false;
                    } else {
                        hasconnection = true;
                    }
                }
            } else {
                hasconnection = true;

            }

            root.showloader();

            sdata.t = root.getcurrenttimestamp();
            jsonurl = root.apiurl + url;
            if (hasconnection) {
                $http({ method: "GET", url: jsonurl, cache: false, params: sdata, timeout: 155000 })
                    .then(function (results) {
                        root.hideloader();
                        callsucess(results.data);
                    }, function (error) {
                        root.hideloader();
                        root.msg("Error Processing your Request");
                    });
            }

        }

        root.checkconnection = function () {
            connected = "notconnected";
            if (navigator.connection.type == Connection.NONE) {
                root.msg("Error establishing Internet Connection");
                connected = "notconnected";

            } else {

                connected = "connected";
            }
            return connected;

        }

        root.postform = function (url, sdata, callsucess, callerror) {

            hasconnection = false;
            if (window.cordova) {
                if (window.Connection) {
                    if (navigator.connection.type == Connection.NONE) {
                        hasconnection = false;
                        root.msg("Error establishing Internet Connection");
                        return false;
                    } else {
                        hasconnection = true;
                    }
                }
            } else {
                hasconnection = true;

            }

            root.showloader();
            jsonurl = root.apiurl + url;

            if (hasconnection) {
                $http.post(jsonurl, sdata, {

                    cache: false,
                    timeout: 155000,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    }
                })
                    .success(function (results) {
                        root.hideloader();

                        callsucess(results);
                    })
                    .error(function (data, status, header, config) {
                        root.hideloader();
                        if (data.error) {
                            root.msg(data.error);
                        } else {
                            root.msg("Error Processing your Request");
                        }
                        callerror(data);
                    })
            }
        }
        root.gotopage = function (url) {
            $state.go(url);
        }
        root.getcurrenttimestamp = function () {
            var currentTime = new Date();
            var n = currentTime.getTime();
            return "t=" + n;
        }
        root.getdt = function (TextBox) {
            exitingDate = $(TextBox).val();
            if (exitingDate == '') {
                var dtoptions = {
                    date: new Date(),
                    'mode': 'date'
                };
            } else {
                var dtoptions = {
                    date: new Date(exitingDate),
                    'mode': 'date'
                };
            }
            $cordovaDatePicker
                .show(dtoptions)
                .then(function (date) {
                    if (date !== undefined) {
                        $(TextBox).val(root.convertDate(date));
                    } else {
                        $(TextBox).val(exitingDate);
                    }
                });
        }

        root.convertDate = function (inputFormat) {
            function pad(s) {
                return (s < 10)
                    ? '0' + s
                    : s;
            }
            var d = new Date(inputFormat);
            v = [
                pad(d.getMonth() + 1),
                pad(d.getDate()),
                d.getFullYear()
            ].join('/');
            return v;
        }
        root.convertTime = function (inputFormat) {
            function pad(s) {
                return (s < 10)
                    ? '0' + s
                    : s;
            }
            var d = new Date(inputFormat);
            v = [
                pad(d.getHours()),
                pad(d.getMinutes())
            ].join(':');
            return v;
        }

        root.replaceNum = function (txt) {
            txt = $.trim(txt);
            txt = txt.replace(/(\.\d+\ )+/, '');
            return txt;
        }

        root.rpt = function (id) {

            var hideSheet = $ionicActionSheet.show({
                buttons: [
                    {
                        text: 'Yes'
                    }, {
                        text: 'No'
                    }
                ],
                titleText: 'Do you want to report this item',
                cancelText: 'Cancel',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    if (index == 0) {

                        url = "/report/" + id + "/" + user.id;
                        root.calljson(url, {}, function (data) {
                            if (data.message == "success") {
                                root.msg("Item reported");

                            }
                        });
                    }
                    return true;
                }
            });

        }

        return root;
    })
    .filter('numberFixedLen', function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = '' + num;
            while (num.length < len) {
                num = '0' + num;
            }
            return num;
        }

    })
    .filter('toDate', function () {
        return function (input) {
            var t = input.split(/[- :]/);
            var date = new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));
            return date;
        };
    });